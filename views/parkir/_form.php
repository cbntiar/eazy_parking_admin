<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Parkir $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="parkir-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kendaraan_id')->textInput() ?>

    <?= $form->field($model, 'waktu_checkin')->textInput() ?>

    <?= $form->field($model, 'waktu_checkout')->textInput() ?>

    <?= $form->field($model, 'tanggal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
