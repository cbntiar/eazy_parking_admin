<?php

use app\models\Parkir;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */

$this->title = 'Eazy Parking';
?>
<div class="site-index">

    <div class="body-content">
        <div class="row">
            <div class="col-md-4">
                <div id="parking-request">

                </div>
            </div>
            <div class="col-md-8">
                <h3 class="">Dashboard</h3>
                <button class="btn btn-primary" onclick="window.location.reload()">Refresh</button>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        'nomor_kendaraan',
                        'waktu_checkin',
                        'waktu_checkout',
                        'tanggal',
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, Parkir $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'id' => $model->id]);
                            }
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>

<script>
    // setInterval(getData(), 1000);

    getData()

    function getData() {
        $.ajax({
            url: "<?= Url::to(['/site/get-data']) ?>",
            type: 'GET',
            dataType: 'HTML',
            beforeSend: function() {
               
            },
            success: function(response) {
                $('#parking-request').html(response);
                // getData()
                setTimeout(function(){getData();}, 500);
            },
            complete: function() {

            },
            error: function(xhr, ajaxOptions, thrownError) {
                var pesan = xhr.status + " " + thrownError + "\n" + xhr.responseText;
                
            }
        });
    }
</script>