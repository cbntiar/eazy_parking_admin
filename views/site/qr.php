<?php

/** @var yii\web\View $this */

$this->title = 'Eazy Parking';
?>
<div class="site-index">

    <div class="text-center bg-transparent mt-1">
        <h3 class="">Scan QR</h3>
    </div>

    <div class="body-content">
        <div class="text-center">
            <img width="500px" style="border: 10px solid #f0f0f0;border-radius: 5px" src="<?= Yii::getAlias('@web') ?>/images/qr.png" />
        </div>
    </div>
</div>
