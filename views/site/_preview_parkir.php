<?php

use yii\widgets\DetailView;

?>

<div class="text-center">
    <h3 class="">Parkir</h3>
    <h3 class="">Permintaan <?= empty($data->waktu_checkout) ? 'Checkin' : 'Checkout' ?></h3>
    <div>
    <?= DetailView::widget([
            'model' => $data,
            'attributes' => [
                'nomor_kendaraan',
                'foto_stnk', 
                'waktu_checkin',
                'waktu_checkout',
            ],
        ]); ?>
    </div>
</div>
