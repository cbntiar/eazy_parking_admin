<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\UserData $model */

$this->title = 'Update User Data: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-success" role="alert">
        Silahkan Kosongkan Password apabila tidak ingin mengubah nya
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'mahasiswa' => $mahasiswa,
    ]) ?>

</div>