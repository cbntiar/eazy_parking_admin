<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\UserData $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div style="width:50%">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'username',
                // 'auth_key',
                // 'password_hash',
                // 'password_reset_token',
                'email:email',
                // 'status',
                [
                    'attribute' => 'status',
                    'value' => function ($data) {
                        $arrayData = ['10' => 'Active', '9' => 'Non Active'];
                        return $arrayData[$data->status];
                    }
                ],
                //'created_at
                // 'created_at',
                // 'updated_at',
                // 'verification_token',
            ],
        ]) ?>

        <?= DetailView::widget([
            'model' => $mahasiswa,
            'attributes' => [
                'npm',
                'nama',
                'jurusan',
            ],
        ]) ?>
    </div>

</div>