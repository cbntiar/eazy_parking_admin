<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\UserData $model */
/** @var yii\widgets\ActiveForm $form */
?>

<style>
    input[readonly] {
        background-color: #eee !important;
    }
</style>

<div class="user-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary([$model, $mahasiswa]); ?>

    <?= $form->field($mahasiswa, 'npm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($mahasiswa, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($mahasiswa, 'jurusan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        ['10' => 'Active', '9' => 'Non Active']
    ); ?>


    <?php /*
    <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'verification_token')->textInput(['maxlength' => true]) ?>
    */ ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS
    $("#mahasiswa-npm").on("blur", function() {
        $("#userdata-username").val($(this).val());
    });
JS;
$this->registerJs($script);
?>