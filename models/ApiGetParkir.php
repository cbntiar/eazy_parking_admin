<?php

namespace app\models;

use yii\base\Model;

class ApiGetParkir extends Model
{
    public $id, $kendaraan_id, $tanggal, $user_id, $single, $page, $recordPerPage;

    public function rules()
    {
        return [
            [['id', 'kendaraan_id', 'tanggal', 'user_id', 'single', 'page', 'recordPerPage'], 'safe'],
            // [['page', 'recordPerPage'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        $attributeLabel =  [
            'id' => 'id',
            'kendaraan_id' => 'kendaraan_id',
            'tanggal' => 'tanggal',
            'page' => 'page',
            'recordPerPage'      => 'recordPerPage',
        ];

        return $attributeLabel;
    }
}
