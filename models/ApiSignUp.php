<?php

namespace app\models;

use yii\base\Model;

class ApiSignUp extends Model
{
    public $username, $password;

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            // [['username'], 'string', 'max' => 6],
            ['username', 'unique', 'targetClass' => User::className(), 'message' => 'This username has already been taken.'],
        ];
    }

    public function attributeLabels()
    {
        $attributeLabel =  [
            'username' => 'username',
            'password' => 'password',
        ];

        return $attributeLabel;
    }
}
