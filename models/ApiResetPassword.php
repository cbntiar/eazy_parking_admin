<?php

namespace app\models;

use yii\base\Model;

class ApiResetPassword extends Model
{
    public $username, $newpassword;

    public function rules()
    {
        return [
            [['username', 'newpassword'], 'required'],
            // [['username'], 'string', 'max' => 6],
            [['username'], 'checkr'],
        ];
    }

    public function attributeLabels()
    {
        $attributeLabel =  [
            'username' => 'username',
            'newpassword' => 'newpassword',
        ];

        return $attributeLabel;
    }

    public function checkr($attribute)
    {
        $existData = User::findByUsername($this->username);
        if (!$existData) {
            $this->addError($attribute, 'username "' . $this->username . '" not exist');
        }
    }
}
