<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Parkir;

/**
 * ParkirSearch represents the model behind the search form of `app\models\Parkir`.
 */
class ParkirSearch extends Parkir
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kendaraan_id', 'user_id'], 'integer'],
            [['waktu_checkin', 'waktu_checkout', 'tanggal'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Parkir::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'kendaraan_id' => $this->kendaraan_id,
            'waktu_checkin' => $this->waktu_checkin,
            'waktu_checkout' => $this->waktu_checkout,
            'tanggal' => $this->tanggal,
        ]);

        return $dataProvider;
    }


    public function searchForApi($params, $selectData, $single)
    {
        $query = Parkir::find();
        $query->alias('t');
        $query->join('LEFT JOIN', 'kendaraan k', 'k.id = t.kendaraan_id');
        $query->select($selectData);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            't.id' => $this->id,
            't.kendaraan_id' => $this->kendaraan_id,
            't.waktu_checkin' => $this->waktu_checkin,
            't.waktu_checkout' => $this->waktu_checkout,
            't.tanggal' => $this->tanggal,
            'k.user_id' => $this->user_id,
        ]);

        $query->orderBy('t.id DESC, t.waktu_checkin DESC');

        if ($single != null && $single == true) {
            return $query->asArray()->one();
        } else {
            return $dataProvider;
        }
    }
}
