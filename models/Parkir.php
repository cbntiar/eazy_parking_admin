<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parkir".
 *
 * @property int $id
 * @property int $kendaraan_id
 * @property string|null $waktu_checkin
 * @property string|null $waktu_checkout
 * @property string|null $tanggal
 */
class Parkir extends \yii\db\ActiveRecord
{
    public $nomor_kendaraan, $foto_stnk,  $user_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parkir';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kendaraan_id'], 'required'],
            [['kendaraan_id'], 'integer'],
            [['waktu_checkin', 'waktu_checkout', 'tanggal', 'user_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kendaraan_id' => 'Kendaraan ID',
            'waktu_checkin' => 'Waktu Checkin',
            'waktu_checkout' => 'Waktu Checkout',
            'tanggal' => 'Tanggal',
        ];
    }
}
