<?php

namespace app\models;

use yii\base\Model;

class ApiGetKendaraan extends Model
{
    public $user_id, $nomor_kendaraan, $page, $recordPerPage;

    public function rules()
    {
        return [
            [['user_id', 'nomor_kendaraan', 'page', 'recordPerPage'], 'safe'],
            [['page', 'recordPerPage'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        $attributeLabel =  [
            'user_id' => 'User ID',
            'nomor_kendaraan' => 'Nomor Kendaraan',
            'page' => 'page',
            'recordPerPage'      => 'recordPerPage',
        ];

        return $attributeLabel;
    }
}
