<?php

namespace app\models;

use yii\base\Model;

class ApiSetParkir extends Model
{
    public $id, $type, $kendaraan_id, $time;

    public function rules()
    {
        return [
            [['id', 'type', 'kendaraan_id', 'time'], 'safe'],
            [['type', 'kendaraan_id', 'time'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        $attributeLabel =  [
            'id' => 'id',
            'type' => 'type',
            'kendaraan_id' => 'kendaraan_id',
            'time' => 'time',
        ];

        return $attributeLabel;
    }
}
