<?php

namespace app\components;

use Yii;
use app\models\Costcenter;
use app\models\Account;
use app\models\AkrupertanggungandetailSearch;
use app\models\Akrupertanggunganheader;
use app\models\Listaccakru;
use app\models\Vendor;
use app\models\VendorTpIc;
use Exception;
use yii\web\HttpException;

class Logic
{

    public static function errorMesages($model)
    {
        $pesan = [];
        $errors = $model->getErrors();
        foreach ($errors as $key => $value) {
            $pesan[] = $key . " : " . implode(", ", $value);
        }
        return implode(", ", $pesan);
    }
}
