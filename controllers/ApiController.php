<?php

namespace app\controllers;

use app\components\Logic;
use app\models\ApiGetKendaraan;
use app\models\ApiGetParkir;
use app\models\ApiResetPassword;
use app\models\ApiSetParkir;
use app\models\ApiSignUp;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Kendaraan;
use app\models\KendaraanSearch;
use app\models\Mahasiswa;
use app\models\Parkir;
use app\models\ParkirSearch;
use app\models\SignupForm;
use app\models\UserData;
use Exception;
use yii\web\HttpException;

class ApiController extends Controller
{

    public function init()
    {
        parent::init();
        Yii::$app->errorHandler->errorAction = '/api/error';
    }

    public function beforeAction($action)
    {
        // if (in_array($action->id, ['login'])) {
        //     $this->enableCsrfValidation = false;
        // }

        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'sign-up' => ['post'],
                    'login' => ['post'],
                    'reset-password' => ['post'],
                ],
            ],
        ];
    }


    public function actionError()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $exception  =   Yii::$app->getErrorHandler()->exception;
        return [
            'statusCode' => $exception->statusCode,
            'status' => false,
            'message' => $exception->getMessage(),
            'exception' => '',
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['api'];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');

        try {
            $postData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            $model = new LoginForm();
            $model->username = $postData['username'];
            $model->password = $postData['password'];
            if ($model->login()) {

                // $jwt = Yii::$app->jwt;
                // $signer = $jwt->getSigner('HS256');
                // $key = $jwt->getKey();
                // $time = time();
                // $jwtParams = Yii::$app->params['jwt'];

                // $token = $jwt->getBuilder()
                //     ->issuedBy($jwtParams['issuer'])
                //     ->permittedFor($jwtParams['audience'])
                //     ->identifiedBy($jwtParams['id'], true)
                //     ->issuedAt($time)
                //     ->expiresAt($time + $jwtParams['expire'])
                //     ->withClaim('uid', $postData['username'])
                //     // ->withClaim('uid', 100)
                //     ->getToken($signer, $key);

                $existData = UserData::find()->where(['username' => $model->username])->asArray()->one();
                $mahasiswa = Mahasiswa::find()->where(['npm' => $model->username])->asArray()->one();
                if ($mahasiswa) {
                    unset($mahasiswa['id']);
                }

                return [
                    'statusCode' => \Yii::$app->response->statusCode,
                    'status' => true,
                    'message' => 'login success',
                    'exception' => '',
                    'data' => array_merge($existData, $mahasiswa),
                    // 'token' => (string)$token,

                ];
            } else {
                return [
                    'statusCode' => \Yii::$app->response->statusCode,
                    'status' => false,
                    'message' => 'login failed',
                    'exception' => Logic::errorMesages($model),
                ];
            }


            // echo '<pre>';
            // print_r($postData);
            // exit;

        } catch (Exception $e) {
            // echo $e->getMessage();
            throw new HttpException(500, $e->getMessage());
        }
    }

    public function actionSignUp()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');

        try {
            $postData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            $api = new ApiSignUp();
            $api->attributes = $postData;


            if ($api->validate()) {

                $model = new SignupForm();
                $model->username = $api->username;
                $model->password = $api->password;

                if ($model->signup()) {
                    return [
                        'statusCode' => \Yii::$app->response->statusCode,
                        'status' => true,
                        'message' => 'sign-up success',
                        'exception' => '',
                        'data' => [
                            'username' => $api->username,
                            'password' => $api->password,
                        ],
                    ];
                } else {
                    return [
                        'statusCode' => \Yii::$app->response->statusCode,
                        'status' => false,
                        'message' => 'sign-up failed',
                        'exception' => Logic::errorMesages($model),
                    ];
                }
            } else {
                return [
                    'statusCode' => \Yii::$app->response->statusCode,
                    'status' => false,
                    'message' => 'sign-up failed',
                    'exception' => Logic::errorMesages($api),
                ];
            }

            // echo '<pre>';
            // print_r($postData);
            // exit;

        } catch (Exception $e) {
            // echo $e->getMessage();
            throw new HttpException(500, $e->getMessage());
        }
    }

    public function actionResetPassword()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');

        try {
            $postData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            $api = new ApiResetPassword();
            $api->attributes = $postData;


            if ($api->validate()) {

                $user = User::findByUsername($api->username);
                if ($api->newpassword) {
                    $user->setPassword($api->newpassword);
                    $user->generateAuthKey();
                    $user->generateEmailVerificationToken();
                }

                if ($user->save()) {
                    return [
                        'statusCode' => \Yii::$app->response->statusCode,
                        'status' => true,
                        'message' => 'reset password success',
                        'exception' => '',
                        'data' => [
                            'username' => $api->username,
                            'newpassword' => $api->newpassword,
                        ],
                    ];
                } else {
                    return [
                        'statusCode' => \Yii::$app->response->statusCode,
                        'status' => false,
                        'message' => 'reset password failed',
                        'exception' => Logic::errorMesages($user),
                    ];
                }
            } else {
                return [
                    'statusCode' => \Yii::$app->response->statusCode,
                    'status' => false,
                    'message' => 'reset password failed',
                    'exception' => Logic::errorMesages($api),
                ];
            }


            // echo '<pre>';
            // print_r($postData);
            // exit;

        } catch (Exception $e) {
            // echo $e->getMessage();
            throw new HttpException(500, $e->getMessage());
        }
    }

    public function actionGetKendaraan()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');

        try {
            $postData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            $return['status'] = true;
            $return['statusCode'] = \Yii::$app->response->statusCode;
            $return['message'] = 'Success';
            $return['data'] = [];


            $api = new ApiGetKendaraan();
            $api->attributes = $postData;
            $selectData = ['id', 'user_id', 'nomor_kendaraan', 'foto_stnk'];



            if ($api->validate()) {
                // echo '<pre>';
                // print_r($postData);
                // print_r($api);
                // exit;

                $pageFormat = ($api->page - 1); // page di yii2 berawal dari 0;

                $searchModel = new KendaraanSearch();
                $searchModel->user_id = $api->user_id;
                $searchModel->nomor_kendaraan = $api->nomor_kendaraan;
                $dataProvider = $searchModel->searchForApi(Yii::$app->request->queryParams, $selectData);
                $dataProvider->setPagination(['pageSize' => $api->recordPerPage, 'page' => $pageFormat]);
                $totalData = $dataProvider->getTotalCount();
                $data = $dataProvider->getModels();

                $arrayData = [];
                foreach ($data as $d) {
                    $aa = [];
                    foreach ($selectData as $s) {
                        $aa[$s] = $d->$s;
                    }

                    $check = Parkir::find()->where(['kendaraan_id' => $d->id])->orderBy('id DESC')->one();
                    $status = 'checkin';
                    if (!empty($check)) {
                        if (empty($check->waktu_checkout)) {
                            $status = 'checkout';
                        }
                    }

                    $aa['status'] = $status;
                    $aa['checkout_id'] = $check?->id;
                    $arrayData[] = $aa;
                }
                // echo json_encode($arrayData);
                // echo '<pre>';
                // print_r($arrayData);
                // exit;

                $return['data'] = [
                    'totalData' => $totalData,
                    'page' => $api->page,
                    'recordPerPage' => $api->recordPerPage,
                    'records' => $arrayData,
                ];
            } else {
                $return['status'] = false;
                $return['message'] = Logic::errorMesages($api);
            }

            return $return;


            // echo '<pre>';
            // print_r($postData);
            // exit;
        } catch (Exception $e) {
            // echo $e->getMessage();
            throw new HttpException(500, $e->getMessage());
        }
    }

    public function actionGetParkir()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');

        try {
            $postData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            $return['status'] = true;
            $return['statusCode'] = \Yii::$app->response->statusCode;
            $return['message'] = 'Success';
            $return['data'] = [];


            $api = new ApiGetParkir();
            $api->attributes = $postData;
            $selectData = ['t.id', 't.kendaraan_id', 'k.nomor_kendaraan', 'k.foto_stnk', 'k.user_id', 't.waktu_checkin', 't.waktu_checkout', 't.tanggal'];



            if ($api->validate()) {
                // echo '<pre>';
                // print_r($postData);
                // var_dump($api->single);
                // exit;

                if ($api->single != null && $api->single == true) {
                    $searchModel = new ParkirSearch();
                    $searchModel->id = $api->id;
                    $searchModel->kendaraan_id = $api->kendaraan_id;
                    $searchModel->tanggal = $api->tanggal;
                    $dataProvider = $searchModel->searchForApi(Yii::$app->request->queryParams, $selectData, $api->single);
                    if ($dataProvider == null) {
                        $return['data'] = [];
                    } else {
                        $return['data'] = $dataProvider;
                    }
                } else {

                    $pageFormat = ($api->page - 1); // page di yii2 berawal dari 0;
                    $searchModel = new ParkirSearch();
                    $searchModel->id = $api->id;
                    $searchModel->kendaraan_id = $api->kendaraan_id;
                    $searchModel->tanggal = $api->tanggal;
                    $dataProvider = $searchModel->searchForApi(Yii::$app->request->queryParams, $selectData,  $api->single);
                    $dataProvider->setPagination(['pageSize' => $api->recordPerPage, 'page' => $pageFormat]);
                    $totalData = $dataProvider->getTotalCount();
                    $data = $dataProvider->getModels();

                    $arrayData = [];
                    foreach ($data as $d) {
                        $aa = [];
                        foreach ($selectData as $s) {
                            $exp = explode('.', $s);
                            // var_dump($exp);
                            // $aa[$s] = $d->$s;
                            $key = $exp[1];
                            $aa[$key] = $d->$key;
                        }
                        $arrayData[] = $aa;
                    }
                    // echo json_encode($arrayData);
                    // echo '<pre>';
                    // print_r($arrayData);
                    // exit;

                    $return['data'] = [
                        'totalData' => $totalData,
                        'page' => $api->page,
                        'recordPerPage' => $api->recordPerPage,
                        'records' => $arrayData,
                    ];
                }
            } else {
                $return['status'] = false;
                $return['message'] = Logic::errorMesages($api);
            }

            return $return;


            // echo '<pre>';
            // print_r($postData);
            // exit;
        } catch (Exception $e) {
            // echo $e->getMessage();
            throw new HttpException(500, $e->getMessage());
        }
    }

    public function actionSetParkir()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');

        try {
            $postData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            $return['status'] = true;
            $return['statusCode'] = \Yii::$app->response->statusCode;
            $return['message'] = 'Success';
            $return['data'] = [];


            $api = new ApiSetParkir();
            $api->attributes = $postData;



            if ($api->validate()) {
                // echo '<pre>';
                // print_r($postData);
                // var_dump($api->type);
                // exit;

                if ($api->type == 'checkin') {
                    $parkir = new Parkir();
                    $parkir->waktu_checkin = $api->time;
                    $parkir->waktu_checkout = null;
                } else {
                    $parkir = Parkir::findOne($api->id);
                    $parkir->waktu_checkout = $api->time;
                }

                $parkir->kendaraan_id = $api->kendaraan_id;
                $parkir->tanggal = date("Y-m-d", strtotime($parkir->waktu_checkin));
                if ($parkir->save()) {
                    $return['data'] = $parkir;
                } else {
                    $return['status'] = false;
                    $return['message'] = Logic::errorMesages($parkir);
                }
            } else {
                $return['status'] = false;
                $return['message'] = Logic::errorMesages($api);
            }

            return $return;


            // echo '<pre>';
            // print_r($postData);
            // exit;
        } catch (Exception $e) {
            // echo $e->getMessage();
            throw new HttpException(500, $e->getMessage());
        }
    }


    public function actionManageKendaraan()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');

        try {
            $postData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            $return['status'] = true;
            $return['statusCode'] = \Yii::$app->response->statusCode;
            $return['message'] = 'Success';
            $return['data'] = [];


            $kendaraan = new Kendaraan();
            $kendaraan->attributes = $postData;

            if ($kendaraan->validate()) {
                // echo '<pre>';
                // print_r($postData);
                // var_dump($api->type);
                // exit;

                if ($postData['type'] == 'add') {
                    if ($kendaraan->save()) {
                        $return['data'] = $kendaraan;
                    } else {
                        $return['status'] = false;
                        $return['message'] = Logic::errorMesages($kendaraan);
                    }
                } else if ($postData['type'] == 'edit') {
                    $kendaraan = Kendaraan::findOne($postData['id']);
                    $kendaraan->attributes = $postData;
                    if ($kendaraan->save()) {
                        $return['data'] = $kendaraan;
                    } else {
                        $return['status'] = false;
                        $return['message'] = Logic::errorMesages($kendaraan);
                    }
                } else if ($postData['type'] == 'delete') {
                    $kendaraan = Kendaraan::findOne($postData['id']);
                    if ($kendaraan->delete()) {
                        $return['message'] = 'Success Delete';
                    } else {
                        $return['status'] = false;
                        $return['message'] = Logic::errorMesages($kendaraan);
                    }
                } else {
                    $return['status'] = false;
                    $return['message'] = 'type tidak ada';
                }
            } else {
                $return['status'] = false;
                $return['message'] = Logic::errorMesages($kendaraan);
            }

            return $return;


            // echo '<pre>';
            // print_r($postData);
            // exit;
        } catch (Exception $e) {
            // echo $e->getMessage();
            throw new HttpException(500, $e->getMessage());
        }
    }

    public function actionHistoryParkir()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');

        try {
            $postData = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            $return['status'] = true;
            $return['statusCode'] = \Yii::$app->response->statusCode;
            $return['message'] = 'Success';
            $return['data'] = [];


            $api = new ApiGetParkir();
            $api->attributes = $postData;
            $selectData = ['t.id', 't.kendaraan_id', 'k.nomor_kendaraan', 'k.foto_stnk', 'k.user_id', 't.waktu_checkin', 't.waktu_checkout', 't.tanggal'];



            if ($api->validate()) {
                // echo '<pre>';
                // print_r($postData);
                // var_dump($api->single);
                // exit;

                $pageFormat = ($api->page - 1); // page di yii2 berawal dari 0;
                $searchModel = new ParkirSearch();
                $searchModel->user_id = $api->user_id;
                $dataProvider = $searchModel->searchForApi(Yii::$app->request->queryParams, $selectData,  $api->single);
                $dataProvider->setPagination(['pageSize' => $api->recordPerPage, 'page' => $pageFormat]);
                $totalData = $dataProvider->getTotalCount();
                $data = $dataProvider->getModels();

                $arrayData = [];
                foreach ($data as $d) {
                    $aa = [];
                    foreach ($selectData as $s) {
                        $exp = explode('.', $s);
                        // var_dump($exp);
                        // $aa[$s] = $d->$s;
                        $key = $exp[1];
                        $aa[$key] = $d->$key;
                    }
                    $arrayData[] = $aa;
                }
                // echo json_encode($arrayData);
                // echo '<pre>';
                // print_r($arrayData);
                // exit;

                $arrayHistory = [];
                foreach ($arrayData as $hh) {
                    $checkin = $hh;
                    $checkout = $hh;

                    

                    if ($hh['waktu_checkout'] != '') {
                        $checkout['type'] = 'checkout';
                        $checkout['time'] = $hh['waktu_checkout'];
                        unset($checkout['waktu_checkin']);
                        unset($checkout['waktu_checkout']);
                        $arrayHistory[] = $checkout;
                    } else {
                        // $checkout['type'] = 'checkout';
                        // $checkout['time'] = null;
                        // unset($checkout['waktu_checkin']);
                        // unset($checkout['waktu_checkout']);
                        // $arrayHistory[] = $checkout;
                    }

                    $checkin['type'] = 'checkin';
                    $checkin['time'] = $hh['waktu_checkin'];
                    unset($checkin['waktu_checkin']);
                    unset($checkin['waktu_checkout']);
                    $arrayHistory[] = $checkin;
                }

                $groupingPerTanggal = [];
                foreach ($arrayHistory as $ds) {
                    $k = $ds['tanggal'];
                    // unset($ds['tanggal']);
                    $groupingPerTanggal[$k][] = $ds;
                }

                $groupingPerTanggal2 = [];
                foreach ($groupingPerTanggal as $ks => $dd) {
                    $groupingPerTanggal2[] = [
                        'tanggal' => $ks,
                        'data' => $dd,
                    ];
                    // echo $ks.'<br>';
                }

                // echo '<pre>';
                // print_r($groupingPerTanggal2);
                // exit;

                $return['data'] = [
                    'totalData' => $totalData,
                    'page' => $api->page,
                    'recordPerPage' => $api->recordPerPage,
                    'records' => $groupingPerTanggal2,
                ];
            } else {
                $return['status'] = false;
                $return['message'] = Logic::errorMesages($api);
            }

            return $return;


            // echo '<pre>';
            // print_r($postData);
            // exit;
        } catch (Exception $e) {
            // echo $e->getMessage();
            throw new HttpException(500, $e->getMessage());
        }
    }
}
