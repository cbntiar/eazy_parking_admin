<?php

namespace app\controllers;

use app\models\Mahasiswa;
use app\models\User;
use app\models\UserData;
use app\models\UserDataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserDataController implements the CRUD actions for UserData model.
 */
class UserDataController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all UserData models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserDataSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserData model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $mahasiswa = Mahasiswa::findOne(['npm' => $model]);
        return $this->render('view', [
            'model' => $model,
            'mahasiswa' => $mahasiswa,
        ]);
    }

    /**
     * Creates a new UserData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new UserData();
        $mahasiswa = new Mahasiswa();

        if ($this->request->isPost) {
            if (($model->load($this->request->post()) && $model->validate()) && ($mahasiswa->load($this->request->post()) && $mahasiswa->validate())) {

                $user = new User();
                $user->username = $model->username;
                $user->email = $model->email;
                $user->setPassword($model->password);
                $user->generateAuthKey();
                $user->generateEmailVerificationToken();
                $user->status = $model->status;
                $user->save(false);

                $mahasiswa->save(false);

                return $this->redirect(['view', 'id' => $user->id]);
            }
        } else {
            $model->loadDefaultValues();
            $mahasiswa->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'mahasiswa' => $mahasiswa,
        ]);
    }

    /**
     * Updates an existing UserData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $mahasiswa = Mahasiswa::findOne(['npm' => $model]);

        if ($this->request->isPost && ($model->load($this->request->post()) && $model->validate()) && ($mahasiswa->load($this->request->post()) && $mahasiswa->validate())) {

            $user = User::findByUsername($model->username);
            $user->username = $model->username;
            $user->email = $model->email;
            if ($model->password != '') {
                $user->setPassword($model->password);
                $user->generateAuthKey();
                $user->generateEmailVerificationToken();
            }

            $user->status = $model->status;
            $user->save(false);

            $mahasiswa->save(false);

            return $this->redirect(['view', 'id' => $user->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'mahasiswa' => $mahasiswa,
        ]);
    }

    /**
     * Deletes an existing UserData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $mahasiswa = Mahasiswa::findOne(['npm' => $model]);
        $model->delete();
        $mahasiswa->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return UserData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserData::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
